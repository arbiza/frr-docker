
## This Dockerfile creates a FRRouting image to run on Docker.
##
## Details available at https://bitbucket.org/arbiza/frr-docker
## -----------------------------------------------------------------------------


## Use Debian image
FROM debian:jessie

LABEL mantainer="Lucas Arbiza" \
      description="FFRouting on Docker"


## Kernel Parameters
# Enable IPv6 and IPv4 forwarding
RUN echo 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf && \
    echo 'net.ipv6.conf.all.forwarding = 1' >> /etc/sysctl.conf && \
    echo 'net.ipv6.conf.default.forwarding = 1' >> /etc/sysctl.conf

# Opens BGP TCP socket per VRF
RUN echo 'net.ipv4.tcp_l3mdev_accept = 0' >> /etc/sysctl.conf

## Enable vlan
RUN echo 8021q >> /etc/modules

# Enable MPLS Kernel Modules
#RUN echo mpls_router >> /etc/modules-load.d/modules.conf && \
#    echo mpls_iptunnel >> /etc/modules-load.d/modules.conf

# Enable MPLS Label processing on all interfaces
# (From documentation at http://docs.frrouting.org/en/latest/installation.html)
# I'd better use a boot script for this
#RUN echo 'net.mpls.conf.<if_0>.input = 1' >> /etc/sysctl.conf && \
#    echo 'net.mpls.conf.<if_1>.input = 1' >> /etc/sysctl.conf ...


## Update the system and install required packages
RUN apt-get update && apt-get install -y \
    curl \
    libc-ares2 \
    libjson-c2 \
    libssh-4 \
    logrotate \
    traceroute


## Get and install RTRLIB
RUN curl -O -J -L  https://ci1.netdef.org/artifact/RPKI-RTRLIB/shared/build-39/Debian-8-x86_64-Packages/librtr0_0.5.0-1_amd64.deb && \
    dpkg -i librtr0_0.5.0-1_amd64.deb

## Get and install FRRouting
RUN curl -O -J -L https://github.com/FRRouting/frr/releases/download/frr-6.0/frr_6.0RPKI-1.debian8+1_amd64.deb && \
    dpkg -i frr_6.0RPKI-1.debian8+1_amd64.deb


## Files and permissions
ADD run.bash /etc
ADD config /etc/frr


## Exposing ports allows daemons to be telneted from the host
## Porst exposed: # zebra, bgp, ospf, ospf6
EXPOSE 2601 2602 2603 2604 2605 2606 2608 2609 2610 2611 2612 2613

VOLUME ["/etc/frr"]

ENV PATH "/sbin:/bin:/usr/sbin:/usr/bin"
ENTRYPOINT ["/bin/bash", "/etc/run.bash"]
