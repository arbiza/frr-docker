From the [FRR's page](https://frrouting.org/):

> _"FRRouting (FRR) is an IP routing protocol suite for Linux and Unix platforms which includes protocol daemons for BGP, IS-IS, LDP, OSPF, PIM, and RIP."_

FRR has several [releases](https://github.com/FRRouting/frr/releases); this Docker image runs [RPKI (Resource Public Key Infrastructure)](https://www.arin.net/resources/rpki/) enabled versions - tags indicate the FRR version built.

FRR runs many protocol daemons. See the section _Environment variables_ below to limit the daemons will run.

I built this image for a GNS3-based network laboratory for a BGP course.

# Persisting data

"_write memory_" command saves configuration changes in the FRR directory (/etc/frr) mounted as a Docker volume.

# Usage

## Running

__Interactive mode:__

Running in interactive mode (-it) you will get in the _vtysh_ shell:

> docker run --privileged -ti arbiza/frr

__Background mode:__

> docker run --privileged -d arbiza/frr

When running in the background (_daemon_) you may connect to _vtysh_ or to each protocol daemon individually. __Don't__ use the Docker _attach_ to connect or you'll get into a loop.

The command below connects to the container's _vtysh_:

>  docker exec -ti container_name_or_id  vtysh

In order to connect to a specific daemon, get the container's IP through _docker inspect container-name-or-id_ command then use it in the command below:

> telnet  container's_IP  daemon's_port

Option for daemon's port are:

- zebra:    2601
- RIPd:     2602
- RIPngd:   2603
- OSPFd:    2604
- BGPd:     2605
- OSPF6d:   2606
- ospf:     2607
- ISISd:    2608
- BABELd:   2609
- nhrpd:    2610
- PIMd:     2611
- LDPd:     2612
- EIGRPd:   2613
- bfdd:     2617

The __password__ is _frr_

## Environment variables

__DAEMONS__: Set this variable listing (space separated) the daemons you want the FRR run. Options are:

- zebra
- ripd
- ldpd
- sharpd
- bgpd  
- ripngd
- nhrpd
- pbrd
- ospfd
- isisd
- eigrpd
- ospf6d
- pimd  
- babeld

By default, FRR runs all the daemons above.

In the example below, FRR runs zebra, OSPF and BGP daemons only:

> docker run --privileged -ti -e DAEMONS='zebra ospfd bgpd' arbiza/frr

__uRPF - Reverse Path Forwarding__

FRR doesn't have commands to change RPF configuration. By default all RPF values are set to 0, but if you run this image in GNS3 it will set all values to 1.

As this image is based on Debian Linux, you can use the environment variables to change kernel's uRPF parameters:

- __RPF_all__: changes the net.ipv4.conf.all.rp_filter parameter to enabled (1) or disabled (0). Linux performs an OR operation for _rp\_filter_, it means that when the _all_ parameter is 1, it will run uRPF on all interfaces regardless its individual values.
- __RPF_set_on_iface__: sets the net.ipv4.conf.<iface>.rp_filter parameter to 1 enabling uRPF on that interface regardless what is set for the _all_ parameter.
- __RPF_unset_on_iface__: set net.ipv4.conf.<iface>.rp_filter to 0 disabling uRPF on that interface - the _all_ parameter must be 0 for this to work.

The following example __enables__ uRPF on interfaces eth2 and eth3 only:

> docker run --privileged -d -e RPF_all='0' -e  RPF_set_on_iface='eth2 eth3'  arbiza/frr

The following example __disables__ uRPF on interfaces eth2 and eth3:

> docker run --privileged -d -e RPF_all='0' -e  RPF_unset_on_iface='eth2 eth3' -e  RPF_set_on_iface='eth0 eth1' arbiza/frr

## GNS3

__Importing:__

Import to GNS3 by using the menu _File_ > _Import appliance_ and selecting the file _arbiza-frr.gns3a_.

__Known issues:__

Problems may occur when you reopen a project that uses this image. GNS3 creates a volume for each container using this image, but after closing the project and reopening it something goes wrong. Every time you make changes in the volume by issuing _write memory_ command, export the topology as a _portable project_. No problems have occurred using portable projects.

# Repository

All the files to build this image are available on a [Bitbucket Repository](https://bitbucket.org/arbiza/frr-docker).
