#!/bin/bash -er

# Fix file's permission and ownership before to start Quagga to avoid issues
chown -R frr /etc/frr



# Enable daemons and start FRRouting
D_file='/etc/frr/daemons'

if [ -z "$DAEMONS" ]; then
    echo
    echo ' > DAEMONS environment variable was not set.'
    echo ' > FRRouting starting all the following daemons:'
    echo ' >'
    echo ' >       - zebra   - bgpd    - ospfd   - ospf6d '
    echo ' >       - ripd    - ripngd  - isisd   - pimd   '
    echo ' >       - ldpd    - nhrpd   - eigrpd  - babeld '
    echo ' >       - sharpd  - pbrd '
    echo
    echo

    # Enable all daemons
    /bin/sed -i -e 's/=no/=yes/' $D_file

else
    for d in $DAEMONS; do
        if [ "$d" == 'zebra' ]; then
            /bin/sed -i -e 's/zebra=no/zebra=yes/' $D_file
        elif [ "$d" == 'bgpd' ]; then
            /bin/sed -i -e 's/bgpd=no/bgpd=yes/' $D_file
        elif [ "$d" == 'ospfd' ]; then
            /bin/sed -i -e 's/ospfd=no/ospfd=yes/' $D_file
        elif [ "$d" == 'ospf6d' ]; then
            /bin/sed -i -e 's/ospf6d=no/ospf6d=yes/' $D_file
        elif [ "$d" == 'ripd' ]; then
            /bin/sed -i -e 's/ripd=no/ripd=yes/' $D_file
        elif [ "$d" == 'ripngd' ]; then
            /bin/sed -i -e 's/ripngd=no/ripngd=yes/' $D_file
        elif [ "$d" == 'isisd' ]; then
            /bin/sed -i -e 's/isisd=no/isisd=yes/' $D_file
        elif [ "$d" == 'pimd' ]; then
            /bin/sed -i -e 's/pimd=no/pimd=yes/' $D_file
        elif [ "$d" == 'ldpd' ]; then
            /bin/sed -i -e 's/ldpd=no/ldpd=yes/' $D_file
        elif [ "$d" == 'nhrpd' ]; then
            /bin/sed -i -e 's/nhrpd=no/nhrpd=yes/' $D_file
        elif [ "$d" == 'eigrpd' ]; then
            /bin/sed -i -e 's/eigrpd=no/eigrpd=yes/' $D_file
        elif [ "$d" == 'babeld' ]; then
            /bin/sed -i -e 's/babeld=no/babeld=yes/' $D_file
        elif [ "$d" == 'sharpd' ]; then
            /bin/sed -i -e 's/sharpd=no/sharpd=yes/' $D_file
        elif [ "$d" == 'pbrd' ]; then
            /bin/sed -i -e 's/pbrd=no/pbrd=yes/' $D_file
        fi
    done
fi


# Enable or enforce disable Linux uRPF (default policy)
if [ ! -z "$RPF_all" ]; then

    if [ "$RPF_all" == '0' ]; then
        /sbin/sysctl -w net.ipv4.conf.all.rp_filter=0
        echo
        echo ' > uRPF disabled on all interfaces.'
        echo
    elif [ "$RPF_all" == '1' ]; then
        /sbin/sysctl -w net.ipv4.conf.all.rp_filter=1
        echo
        echo ' > uRPF enabled on all interfaces.'
        echo
    fi
fi

# Enable uRPF on specific interfaces
if [ ! -z "$RPF_set_on_iface" ]; then

    for iface in $RPF_set_on_iface; do
        /sbin/sysctl -w net.ipv4.conf.$iface.rp_filter=1
    done
fi

# Disable uRPF on specific interfaces
if [ ! -z "$RPF_unset_on_iface" ]; then

    for iface in $RPF_unset_on_iface; do
        /sbin/sysctl -w net.ipv4.conf.$iface.rp_filter=0
    done
fi




/usr/lib/frr/frr start

echo
echo
echo '--------------------------------------------------------'
echo
echo ' + Press ENTER to get into VTYsh.'
echo " + Press C^p C^q to exit container's shell"
echo " + Type 'exit' to exit and stop container."
echo "   (When in VTY shell you'll have to type 'exit' twice)"
echo
echo '--------------------------------------------------------'
echo
echo
echo 'Waiting input...'
echo

while [ 1 ]; do
    read any

    if [ "$any" == "exit" ]; then
        break
    else
        /usr/bin/vtysh
    fi
done
