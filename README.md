# FRR on Docker

This repository contains the files to build the [arbiza/frr](https://hub.docker.com/r/arbiza/frr/) Docker image that runs the FRRouting (FRR) IP routing protocol suite. FRR is a fork of the [Quagga Routing Suite](http://www.nongnu.org/quagga/) project.

From the [FRR's page](https://frrouting.org/):

> FRRouting (FRR) is an IP routing protocol suite for Linux and Unix platforms which includes protocol daemons for BGP, IS-IS, LDP, OSPF, PIM, and RIP.

The branches match FRR versions. Master branch is always on the latest version.

# Usage

You don't need to build this image to use it; it's available on [Docker Hub](https://hub.docker.com/r/arbiza/frr/) with usage instructions.


# Building

In case you want to build with some pre-defined configuration you may add your configurations it in the files inside the __config/__ directory. Some configurations may also be set when the container starts (_See the usage instructions on [Docker Hub](https://hub.docker.com/r/arbiza/frr/)_).

Build it with the following command:

```
git clone https://arbiza@bitbucket.org/arbiza/frr-docker.git
cd frr-docker
docker build -t <name> .
```
